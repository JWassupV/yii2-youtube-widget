<?
namespace jwassupv\widgets\youtube;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;
use yii\widgets\InputWidget;

class YtUploadInputWidget extends InputWidget
{
    public $width = '400';
    public $sendUrl;
    public $sendMethod = 'POST';
    public $alert = "'Видео ' + event.data.videoId + ' было успешно загруженно. Опобликуйте видео для того что бы его могли видеть другие пользователи'";
    public $videoTitle;
    public $videoDescription;
    public $videoKeywords;
    public $videoPrivacy = 'public';

    public function init(){
        parent::init();

        $view = $this->getView();

        $view->registerJsFile('https://www.youtube.com/iframe_api');
        $view->registerJs(
//            "var tag = document.createElement('script');".
//            "tag.src = \"https://www.youtube.com/iframe_api\";".
//            "var firstScriptTag = document.getElementsByTagName('script')[0];".
//            "firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);".
            'var widget;'.
            'var player;'.
            "var inputName = '".($this->model ? Html::getInputName($this->model, $this->attribute) : $this->name)."';".
            'function onYouTubeIframeAPIReady() {'.
                "widget = new YT.UploadWidget('{$this->id}', {".
                    "width: {$this->width},".
                    "events: {".
                        "'onUploadSuccess': onUploadSuccess,".
                        "'onProcessingComplete': onProcessingComplete,".
                        "'onApiReady': onApiReady".
                    "}".
                "});".
            "}".
            "function onUploadSuccess(event) {".
                "alert({$this->alert});".
                "jQuery('input[name=\"'+inputName+'\"]').val(event.data.videoId);".
            "}".
            "function onProcessingComplete(event) {".
                "player = new YT.Player('{$this->id}-player', {".
                    "height: 390,".
                    "width: 640,".
                    "videoId: event.data.videoId,".
                    "events: {}".
                "});".
                "jQuery('#{$this->id}').hide();".
            "}".
            "function onApiReady(event){".
                "widget.setVideoTitle('{$this->videoTitle}');".
                "widget.setVideoDescription('{$this->videoDescription}');".
                "widget.setVideoKeywords('{$this->videoKeywords}');".
                "widget.setVideoPrivacy('{$this->videoPrivacy}');".
            "}",
            $view::POS_END
        );

        YiiAsset::register($view);

    }

    public function run(){
        $video = Html::tag('div', Html::tag('div', '', ['id'=>$this->id]),['style'=>'text-align:center']).Html::tag('div', '', ['id'=>$this->id.'-player']);

        if($this->model){
            return $video.Html::activeHiddenInput($this->model, $this->attribute);
        }else{
            return $video.Html::hiddenInput($this->name, $this->value);
        }
    }
}