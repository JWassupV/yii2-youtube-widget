<?
namespace jwassupv\widgets\youtube;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;

class YtPlayerWidget extends Widget
{
    public $width = '420';
    public $height = '315';
    public $videoId;

    public function init(){
        parent::init();

        $view = $this->getView();
        /*$view->registerJsFile('https://www.youtube.com/iframe_api');


        $view->registerJs(
            'var player, oldOnYouTubeIframeAPIReady;'.
            "if(onYouTubeIframeAPIReady)".
                "oldOnYouTubeIframeAPIReady = onYouTubeIframeAPIReady;".
            "functon onYouTubeIframeAPIReady(){oldOnYouTubeIframeAPIReady;".
                "player = new YT.Player('{$this->id}', {".
                    "height: {$this->height},".
                    "width: {$this->width},".
                    "videoId: '{$this->videoId}',".
                    "events: {}".
                "});".
            "}",
            $view::POS_READY
        );*/

        YiiAsset::register($view);

    }

    public function run(){
        return Html::tag('iframe', '',['width'=>$this->width, 'height'=>$this->height, 'src'=>"https://www.youtube.com/embed/{$this->videoId}"]);
//        return '<iframe width="420" height="315" src="https://www.youtube.com/embed/Ahg6qcgoay4" frameborder="0" allowfullscreen></iframe>';
//        return Html::tag('div', '', ['id'=>$this->id]);
    }
}