<?
namespace jwassupv\widgets\youtube;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;

class YtUploadWidget extends Widget
{
    public $width = '500';
    public $sendUrl;
    public $sendMethod = 'POST';
    public $alert = "'Видео ' + event.data.videoId + ' было добавленно. После модерации оно появится на сайте'";

    public function init(){
        parent::init();

        $view = $this->getView();

        YiiAsset::register($view);

        $view->registerJs(
            "var tag = document.createElement('script');".
            "tag.src = \"https://www.youtube.com/iframe_api\";".
            "var firstScriptTag = document.getElementsByTagName('script')[0];".
            "firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);".
            'var widget, oldOnYouTubeIframeAPIReady;'.
            "if(onYouTubeIframeAPIReady){".
                "oldOnYouTubeIframeAPIReady = onYouTubeIframeAPIReady;}".
            "function YtUpload{$this->id}() {".
                "widget = new YT.UploadWidget('{$this->id}', {".
                    "width: {$this->width},".
                    "events: {".
                        "'onUploadSuccess': onUploadSuccess,".
                        //"'onProcessingComplete': onProcessingComplete".
                    "}".
                "});".
            "}".
            "functon onYouTubeIframeAPIReady(){oldOnYouTubeIframeAPIReady; YtUpload{$this->id}}".
            "function onUploadSuccess(event) {".
                "alert({$this->alert});".
                ($this->sendUrl ?
                    "jQuery.ajax({".
                        "url: '".Url::toRoute($this->sendUrl)."',".
                        "method: '{$this->sendMethod}'".
                        "data: {id: event.data.videoId}".
                    "});"
                : null).
            "}",
            $view::POS_END
        );
    }

    public function run(){
        return Html::tag('div', '', ['id'=>$this->id]);
    }
}